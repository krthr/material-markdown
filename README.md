# Welcome to Material Markdown!
This is a simple and clean markdown editor with side by side preview.

![Screenshot](https://lh3.googleusercontent.com/nsP_Jyz0Szlf3XKs2PV-e-ySn-1CuXw2ciVa0VDU9E3rJ9y0AVIx8SQAJojugA4dcCTIeI99gW4=s1280-h800-e365-rw)

**Shortcuts**
- Load Sample Page: Ctrl+M
 - Mac: Cmd+4
- Open File: Ctrl+Q
	- Mac: Cmd+5
- Save File: Ctrl+Shift+1
	- Mac: Cmd+2
- Save As File: Ctrl+Shift+Y
	- Mac: Cmd+3
- Toggle Blockquote: Ctrl+'
- Toggle Bold: Ctrl+B
- Toggle Italic: Ctrl+I
- Draw Link: Ctrl+K
- Toggle Unordered List: Ctrl+L
- Toggle Blockquote: Ctrl+'
- Toggle Bold: Ctrl+B
- Toggle Italic: Ctrl+I
- Draw Link: Ctrl+K
- Toggle Unordered List: Ctrl+L

> This app uses the open source SimpleMDE markdown editor